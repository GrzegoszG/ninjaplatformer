﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    public float moveSpeed;
    public float jumpForce;
    public float maxSpeed;
    public float maxHp;
    public float hp;
    public float flashTime;
    public int id;
    public AudioSource heart;
    public AudioSource shu;

    private Rigidbody2D rb;
    private SpriteRenderer sprite;
    private Color color;

    public KeyCode left;
    public KeyCode right;
    public KeyCode jump;
    public KeyCode shoot;
    public KeyCode special;

    private float speed;
    private Vector2 force;
    private float flash;
    private bool ded;
    private int deaths;
    private Vector3 startPoint;

    public Transform groundCheckPoint;
    public float groundCheckRadius;
    public LayerMask whatIsGround;

    private bool canShoot;
    private bool isGrounded;
    private Animator anim;
    public Transform throwPoint;
    public GameObject shuriken;

	// Use this for initialization
	void Start () {
        startPoint = transform.position;
        rb = GetComponent<Rigidbody2D>();
        speed = moveSpeed;
        force.x = 0;
        force.y = 0;
        anim = GetComponent<Animator>();
        hp = maxHp;
        sprite = GetComponent<SpriteRenderer>();
        canShoot = true;
        ded = false;
        deaths = 0;
        color = sprite.color;
        if (id == 2)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
	}
	
	// Update is called once per frame
	void Update () {
        if(ded==false)
        {
            if (hp <= 0)
            {
                deaths++;
                ded = true;
                speed = moveSpeed / 10;
                canShoot = false;
                sprite.color = Color.red;
                sprite.color = new Color(sprite.color.r, sprite.color.b, sprite.color.b, 128);
            }
        }
        isGrounded = Physics2D.OverlapCircle(groundCheckPoint.position , groundCheckRadius, whatIsGround);

        if (Input.GetKeyDown(shoot) && canShoot)
        {
            GameObject szur = (GameObject)Instantiate(shuriken, throwPoint.position, transform.rotation);
            szur.transform.localScale = new Vector3(transform.localScale.x, 1, 1);
            if (isGrounded)
            {
                force = new Vector2(-1 * 20 * moveSpeed * transform.localScale.x, 0);
                rb.AddForce(force);
            }
            shu.Play();
        }
        
        if (Input.GetKeyDown(special) && !isGrounded)
        {
            rb.velocity = new Vector2(-moveSpeed/10, rb.velocity.y);
            force.x = 0;
            force.y = -50;
            rb.AddForce(force);
            force.y = 0;
        }
        else if (Input.GetKey(left))
        {
            if (rb.velocity.x > -maxSpeed)
            {
                force.x = -moveSpeed;
                force.y = 1;
                rb.AddForce(force);
                force.y = 0;
            }
                //rb.velocity = new Vector2(-moveSpeed, rb.velocity.y);
        }
        else if (Input.GetKey(right))
        {
            if (rb.velocity.x < maxSpeed)
            {
                force.x = moveSpeed;
                force.y = 1;
                rb.AddForce(force);
                force.y = 0;
            }
            //rb.velocity = new Vector2(moveSpeed, rb.velocity.y);
        }
        else
            rb.velocity = new Vector2(0, rb.velocity.y);
        if (Input.GetKeyDown(jump)&& isGrounded)
        {
            speed += 15;
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
        }
        else
        {
            speed = moveSpeed;
        }
        if (rb.velocity.x < 0)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
        else if (rb.velocity.x > 0)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
        anim.SetFloat("Speed", Mathf.Abs(rb.velocity.x));
        anim.SetBool("Grounded", isGrounded);
        if(Input.GetKeyDown(shoot))
        anim.SetTrigger("Throw");
        else
        anim.ResetTrigger("Throw");
    }

    public void Hurt(float damage)
    {
        Color col = sprite.color;
        hp -= damage;
        if (hp < 3)
        {
            heart.Play();
        }
        if (flash > 0)
        {
            flash -= Time.deltaTime;
            
            sprite.color = col;
        }
        float r = Random.value;
        if (r < 0.33)
        {
            heart.Play();
        }
    }

    public int getDeaths()
    {
        return deaths;
    }

    public void setDed(bool x)
    {
        ded = x;
    }

    public bool getDed()
    {
        return ded;
    }

    public void Revive()
    {
        transform.position = startPoint;
        hp = maxHp;
        sprite.color = color;
        canShoot = true;
        ded = false;
        heart.Play();
    }
}
