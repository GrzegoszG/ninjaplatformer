﻿using UnityEngine;
using System.Collections;

public class Shuriken : MonoBehaviour {

    public float speed;
    public float damage;
    public GameObject fx;
    private Rigidbody2D rb;
    private PlayerController target;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(0, 0, -transform.localScale.x*speed);
        rb.velocity = new Vector2(speed * transform.localScale.x, 0);
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            target = other.gameObject.GetComponent<PlayerController>();
            target.Hurt(damage);
        }
        
        if (!other.gameObject.Equals(gameObject))
        {
            Instantiate(fx, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }
}
